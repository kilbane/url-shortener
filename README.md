# URL Shortener
## Dependencies
- Docker
- Docker Compose

## Usage
Create a `.env` file with the following variables, inserting your own PostgreSQL username and password:
- `POSTGRES_DB="docker-nest-postgres"`
- `POSTGRES_USER="<username>"`
- `POSTGRES_PASSWORD="<password>"`
- `DATABASE_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@localhost:5432/${POSTGRES_DB}?schema=public"`
- `REDIS_HOST = "cache"`
- `REDIS_PORT = "6379"`

Then run `docker compose up` to build and start the containers. You can access
the Swagger UI at `localhost/api` to test the api endpoints. For the URL get
request, you'll need to make the request via the address bar of your browser to
be redirected to the full URL.
