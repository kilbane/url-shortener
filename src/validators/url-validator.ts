import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import url from 'node:url';

@ValidatorConstraint()
export class UrlValidator implements ValidatorConstraintInterface {
  validate(text: string) {
    try {
        const myURL = new URL(text);
        return true;
    } catch (err) {
        return false;
    }
  }
}
