import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { CacheInterceptor } from '@nestjs/cache-manager';
import { RequestsService } from '../requests/requests.service';

@Controller('stats')
@ApiTags('Stats')
export class StatsController {
  constructor(private readonly requestsService: RequestsService) {}

  @Get()
  @ApiOkResponse({ description: 'Stats returned successfully.'})
  async getStats(): Promise<object> {
    return this.requestsService.requests();
  }
}
