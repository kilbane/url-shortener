import { Module } from '@nestjs/common';
import { RequestsService } from '../requests/requests.service';
import { PrismaService } from '../prisma.service';
import { StatsController } from './stats.controller';

@Module({
  providers: [RequestsService, PrismaService],
  controllers: [StatsController],
  exports: [PrismaService],
})
export class StatsModule {}
