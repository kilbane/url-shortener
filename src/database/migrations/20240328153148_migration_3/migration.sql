/*
  Warnings:

  - Added the required column `responseStatusCode` to the `Request` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Request" DROP CONSTRAINT "Request_short_fkey";

-- AlterTable
CREATE SEQUENCE request_id_seq;
ALTER TABLE "Request" ADD COLUMN     "responseStatusCode" INTEGER NOT NULL,
ALTER COLUMN "id" SET DEFAULT nextval('request_id_seq'),
ALTER COLUMN "timestamp" SET DEFAULT CURRENT_TIMESTAMP;
ALTER SEQUENCE request_id_seq OWNED BY "Request"."id";
