-- CreateTable
CREATE TABLE "Request" (
    "id" INTEGER NOT NULL,
    "method" TEXT NOT NULL,
    "timestamp" TIMESTAMP(3) NOT NULL,
    "short" TEXT NOT NULL,

    CONSTRAINT "Request_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Request" ADD CONSTRAINT "Request_short_fkey" FOREIGN KEY ("short") REFERENCES "Url"("short") ON DELETE RESTRICT ON UPDATE CASCADE;
