-- CreateTable
CREATE TABLE "Url" (
    "short" TEXT NOT NULL,
    "full" TEXT NOT NULL,

    CONSTRAINT "Url_pkey" PRIMARY KEY ("short")
);
