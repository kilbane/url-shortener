import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Validate } from 'class-validator';
import { UrlValidator } from '../validators/url-validator';

export class Url {
  @ApiProperty({ type: String })
  @Validate(UrlValidator, { message: "Invalid URL."})
  full: string;
  @ApiPropertyOptional({ type: String })
  short: string;
}
