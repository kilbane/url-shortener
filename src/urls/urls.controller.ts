import { Controller, Get, Param, Post, Body, Redirect, NotFoundException, ConflictException, UseInterceptors } from '@nestjs/common';
import { ApiTags, ApiOkResponse, ApiNotFoundResponse, ApiCreatedResponse, ApiUnprocessableEntityResponse, ApiFoundResponse, ApiConflictResponse } from '@nestjs/swagger';
import { CacheInterceptor } from '@nestjs/cache-manager';
import { UrlsService } from './urls.service';
import { RequestsService } from '../requests/requests.service';
import { RequestModel } from '../requests/requests.interface';
import { Prisma, Url as UrlModel} from '@prisma/client';
import { Url as UrlDTO } from './urls.interface';

@Controller('urls')
@ApiTags('URLs')
export class UrlsController {
  constructor(private readonly urlsService: UrlsService, private readonly requestsService: RequestsService) {}

  @Get(':short')
  @UseInterceptors(CacheInterceptor)
  @ApiFoundResponse({ description: 'URL found, redirecting.' })
  @ApiNotFoundResponse({ description: 'URL not found.' })
  @Redirect()
  async getUrlByShort(@Param('short') short: string): Promise<{ url: string }> {
    const url = await this.urlsService.url(short);
    if(url === null) {
      const request: RequestModel = { method: 'get', responseStatusCode: 404, short: short };
      this.requestsService.logRequest(request);
      throw new NotFoundException('URL not found.'); 
    }
    const request: RequestModel = { method: 'get', responseStatusCode: 302, short: short };
    this.requestsService.logRequest(request);
    return { url: url["full"] };
  }

  @Post()
  @ApiCreatedResponse({ description: 'URL created successfully.' })
  @ApiConflictResponse({ description: 'Shortened URL already exists.' })
  async createUrl(
    @Body() url: UrlDTO,
  ): Promise<UrlModel> {
    try {
      const createdUrl = await this.urlsService.createUrl(url);
      const request: RequestModel = { method: 'post', responseStatusCode: 201, short: createdUrl["short"] };
      this.requestsService.logRequest(request);
      return createdUrl;
    } catch (error) {
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          const request: RequestModel = { method: 'post', responseStatusCode: 409, short: url["short"] };
          this.requestsService.logRequest(request);
          throw new ConflictException('Shortened URL already exists.');
        }
      }
    }
  }
}
