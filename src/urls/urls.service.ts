import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { Url, Prisma } from '@prisma/client';
import { Url as UrlDTO } from './urls.interface';


@Injectable()
export class UrlsService {
  constructor(private prisma: PrismaService) {}

  async url(
    short: string,
  ): Promise<Url|null> {
    return this.prisma.url.findUnique({
      where: {
        short: short
      },
    });
  }

  async createUrl(data: UrlDTO): Promise<Url> {
    return this.prisma.url.create({
      data,
    });
  }
}
