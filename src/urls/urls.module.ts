import { Module } from '@nestjs/common';
import { UrlsService } from './urls.service';
import { RequestsService } from '../requests/requests.service';
import { PrismaService } from '../prisma.service';
import { UrlsController } from './urls.controller';

@Module({
  providers: [UrlsService, RequestsService, PrismaService],
  controllers: [UrlsController],
  exports: [UrlsService, PrismaService],
})
export class UrlsModule {}
