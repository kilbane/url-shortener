import { Module } from '@nestjs/common';
import { RequestsService } from './requests.service';
import { PrismaService } from '../prisma.service';

@Module({
  providers: [RequestsService, PrismaService],
  controllers: [],
  exports: [RequestsService, PrismaService],
})
export class RequestsModule {}
