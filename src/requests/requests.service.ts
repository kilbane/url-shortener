import { Injectable, Logger } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { Request, Prisma } from '@prisma/client';
import { RequestModel } from './requests.interface';


@Injectable()
export class RequestsService {
  constructor(private prisma: PrismaService) {}
  private readonly logger = new Logger(RequestsService.name);

  async requests(): Promise<object | null> {
    const day = await this.statsByPeriod(24 * 60 * 60 * 1000);
    const week = await this.statsByPeriod(7 * 24 * 60 * 60 * 1000);
    const year = await this.statsByPeriod(365 * 24 * 60 * 60 * 1000);
    const stats = {
      day: day,
      week: week,
      year: year
    };
    return stats;
  }

  async logRequest(data: RequestModel): Promise<Request> {
    const request: Request = await this.prisma.request.create({
      data
    });
    this.logger.log(request);
    return request;
  }

  private async statsByPeriod(period: number): Promise<object | null> {
    const requests = await this.prisma.request.groupBy({
      by: ['method', 'responseStatusCode'],
      where: {
        timestamp: {
          lte: new Date(new Date().getTime()),
          gte: new Date(new Date().getTime() - period),
        }
      },
      _count: {
        _all: true
      }
    });
    this.logger.log(requests);
    var stats: object = {}
    for(var group of requests) {
      var method = group["method"];
      var code = group["responseStatusCode"];
      var count = group["_count"]["_all"];
      if(stats[method] === undefined) {
        stats[method] = {};
      }
      stats[method][code] = count;
    }
    this.logger.log(stats);
    return stats;
  }
}
