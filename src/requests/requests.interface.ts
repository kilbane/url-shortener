export class RequestModel {
  method: string;
  responseStatusCode: number;
  short: string;
}
